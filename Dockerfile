FROM docker.io/library/postgres:17.4-bookworm

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-17-ip4r \
    && rm -rf /var/lib/apt/lists/*
